﻿#include <iostream>
using namespace std;

class Animal
{
public:
    virtual void voice()
    const = 0;
};

class Cat : public Animal
{
public:
    void voice() const override
    {
        cout << "Meow\n";
    }
};

class Dog : public Animal
{
public:
    void voice() const override
    {
        cout << "Woof\n";
    }
};

class Maus : public Animal
{
public:
    void voice() const override
    {
        cout << "Pi\n";
    }
};
int main()
{
    Animal* animals[3];
    animals[0] = new Cat();
    animals[1] = new Dog();
    animals[2] = new Maus();

    for (Animal* a : animals)
        a->voice();
}